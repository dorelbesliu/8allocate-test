<?php

namespace App\Http\Controllers;

use App\Costumer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
	/**
	 * Customer create page.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {
		return view('customer.create');
	}

	/**
	 * Store customer data.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request) {
		$request->validate([
			'name' => 'required|min:3',
			'cnp' => 'required|size:13'
		]);

		$customer = new Costumer();
		$customer->name = $request->name;
		$customer->cnp = $request->cnp;
		$customer->save();

		return response()->json([
			'customerId' => $customer->id
		]);
	}

	/**
	 * Search costumer by name.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function costumersData(Request $request) {
		$costumers = Costumer::where('name', 'like', '%' . $request->keywords . '%')->get();

		return response()->json([
			'costumers' => $costumers
		]);
	}
}
