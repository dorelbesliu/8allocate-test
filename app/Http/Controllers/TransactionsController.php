<?php

namespace App\Http\Controllers;

use App\Costumer;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
	/**
	 * Show transaction page.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('transaction.transactions');
	}

	/**
	 * Transaction create page.
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create() {

		return view('transaction.create');
	}

	/**
	 * Store customer data.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request) {
		$request->validate([
			'costumerId' => 'required',
			'amount' => 'required|regex:/^\d*(\.\d{1,2})?$/'
		]);

		$customer = Costumer::find($request->costumerId);

		if (empty($customer)) {
			return response()->json([
				'message' => 'Costumer was not found.'
			], 500);
		}

		$transaction = new Transaction();
		$transaction->costumer_id = $request->costumerId;
		$transaction->amount = $request->amount;
		$transaction->date =  date('Y-m-d');
		$transaction->save();

		return response()->json([
			'transaction' => [
				'transactionId' => $transaction->id,
				'customerId' => $customer->id,
				'amount' => $transaction->amount,
				'date' => $transaction->date
			]
		]);
	}

	/**
	 * Filter transactions.
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function transactionsData(Request $request) {
		$transactionsQuery = Transaction::with(['costumer']);

		if (!empty($request->keywords)) {
			$transactionsQuery->where(\DB::raw('CONCAT(customer_id, amount, date)'),
				'LIKE', "%" . $request->keywords . "%");
		}

		if (!empty($request->fromRows)) {
			$transactionsQuery->take(Transaction::count())->skip($request->fromRows);
		}

		if (!empty($request->toRows)) {
			$transactionsQuery->take($request->toRows);
		}

		$transactions = $transactionsQuery->get();

		return response()->json([
			'transactions' => $transactions
		]);
	}
}
