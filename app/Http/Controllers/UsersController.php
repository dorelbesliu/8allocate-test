<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
	/**
	 * Get current logged user.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function current()
	{
		$user = Auth::user();

		if (empty($user)) {
			return response()->json([
				'message' => 'User not found',
			], 500);
		}

		return response()->json([
			'user' => $user
		]);
	}
}
