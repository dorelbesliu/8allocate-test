<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	/**
	 * Relation belongs to with customers table.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function costumer() {
		return $this->belongsTo(Costumer::class, 'costumer_id', 'id');
	}
}
