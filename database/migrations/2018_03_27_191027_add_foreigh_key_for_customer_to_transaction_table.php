<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeighKeyForCustomerToTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('transactions', function (Blueprint $table) {
		    $table->foreign( 'costumer_id')
		          ->references('id')->on('costumers')
		          ->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('transactions', function (Blueprint $table) {
		    $table->dropForeign( 'transactions_costumer_id_foreign' );
	    });
    }
}
