<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::truncate();

    	User::create([
    		'name' => 'Administrator',
		    'username' => 'admin',
		    'password' => \Illuminate\Support\Facades\Hash::make('secret'),
		    'created_at' => date('Y-m-d H:i:s'),
		    'updated_at' => date('Y-m-d H:i:s')
	    ]);
    }
}
