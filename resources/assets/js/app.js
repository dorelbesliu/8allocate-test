/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import currencyValidator from './currency-validator';
import VueProgressBar from 'vue-progressbar';
import axios from 'axios';
import VueRouter from 'vue-router';
import router from './routes';

Vue.directive('currency', {
    update: function (el, binding) {
        let result = currencyValidator.parse(binding.value, Number(binding.oldValue))
        if (result.warning) {
            el.value = result.value
            binding.value = result.value;
        }

        return result.value
    }
});

const options = {
    progressBar: {
        color: '#00bad6',
        failedColor: '#874b4b',
        thickness: '4px',
        transition: {
            speed: '0.6s',
            opacity: '0.2s'
        },
        autoRevert: true,
        location: 'top'
    }
};

Vue.use(VueProgressBar, options.progressBar);
Vue.use(VueRouter);

/**
 * GLOBAL EVENT HUB
 *
 * For future see Vuex.
 */
window.eventHub = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import app from './components/app.vue';

axios.get('/users/current').then(response => {
    new Vue({
        el: '#app',
        router,
        template: '<app></app>',
        data: {
            constants: appConstants,
            currentUser: response.data.user,
        },
        components: {
            app
        }
    });
});
