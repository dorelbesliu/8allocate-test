import VueRouter from 'vue-router';

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/transactions', name: 'transactions', component: require('./components/transaction/transactions.vue') },
        { path: '/transactions/create', name: 'transactions.create', component: require('./components/transaction/create.vue') },
        { path: '/customers/create', name: 'customers.create', component: require('./components/customers/create.vue') },
        { path: '*', component: require('./components/common/page-not-found.vue') }
    ],
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 };
    }
});

router.beforeEach((to, from, next) => {
    eventHub.$Progress.start();

    next();
});

router.afterEach((to, from) => {
    eventHub.$Progress.finish();
});

export default router;
