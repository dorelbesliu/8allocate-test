<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @if(Auth::user())
        <div id="app"></div>
    @else
        @yield('content')
    @endif

    <!-- Scripts -->
    <script>
        var appConstants = {!! json_encode(config('app')) !!}
            appConstants.base_url = '{!! url('/') !!}'
    </script>
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
