<div class="list-group">
    <a href="{{route('transactions')}}" class="list-group-item list-group-item-action @if(Request::is('transactions')) active @endif">Transactions</a>
    <a href="{{route('transactions.create')}}" class="list-group-item list-group-item-action">Add Transaction</a>
    <a href="{{route('customers.create')}}" class="list-group-item list-group-item-action">Add Costumer</a>
</div>