@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Create Transaction</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            You are on create transaction page!
        </div>
    </div>
@endsection