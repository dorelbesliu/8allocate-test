<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\\LoginController@login');

Route::get('/logout', function () {
	Auth::logout();
	return redirect('/login');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
	Route::get('/transactions', 'TransactionsController@index')->name('transactions');
	Route::get('/transactions/create', 'TransactionsController@create')->name('transactions.create');
	Route::post('/transactions/store', 'TransactionsController@store')->name('transactions.store');
	Route::get('/transactions/data', 'TransactionsController@transactionsData')->name('transactions.data');
	Route::get('/customers/create', 'CustomersController@create')->name('customers.create');
	Route::post('/customers/store', 'CustomersController@store')->name('customers.store');
	Route::get('/customers/data', 'CustomersController@costumersData')->name('customers.data');

	Route::group(['prefix' => 'users'], function () {
		Route::get('/current', 'UsersController@current');
	});
});